import React, { Component } from "react";
import Board from "./Components/Board";


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boardData: "",
    };
  }

  componentDidMount() {
    fetch(
      "https://api.trello.com/1/members/me/boards?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    ).then((response)=>{
      return response.json();
    }).then((data)=>{
      this.setState({boardData: data});
    })
    .catch((err)=>{
      console.error("Error in fetching all boards");
      console.log(err);
    })
  }


  render() {
    return (
      <div className="overflow-hidden">
        {this.state.boardData==="" ? <h3>Loading...</h3> : <Board boardArray={this.state.boardData}/>}
        
      </div>
    );
  }
}
export default App;
