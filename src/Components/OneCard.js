import React, { Component } from "react";
import Modal from "./Modal";

class OneCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: "",
    };
  }

  coverData = (cardId, coverId) => {
    fetch(
      "https://api.trello.com/1/cards/" +
        cardId +
        "/attachments/" +
        coverId +
        "?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa"
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ cover: data.url });
      });
  };

  commentIcon = () => {
    if (this.props.element.badges.comments === 0) {
      return "";
    } else {
      return (
        <div className="mx-1 mb-1 d-inline-block">
          <i className="bi bi-chat-dots mx-1 w-3"></i>
          <span>{this.props.element.badges.comments}</span>
        </div>
      );
    }
  };

  isAttachment = (element) => {
    if (element.cover.idAttachment !== null) {
      if (this.state.cover === "") {
        this.coverData(element.id, element.cover.idAttachment);
        return <p>Loading...</p>;
      } else {
        return (
          <img src={this.state.cover} className="card-img-top" alt="cover" />
        );
      }
    } else if (element.cover.color !== null) {
      return (
        <div
          className="card-img-top"
          style={{ backgroundColor: element.cover.color, height: "35px" }}
        ></div>
      );
    } else if (element.cover.idUploadedBackground !== null) {
      return (
        <img
          src={element.cover.sharedSourceUrl}
          className="card-img-top"
          alt="cover"
          style={{height: "150px"}}
        />
      );
    } else {
      return "";
    }
  };

  render() {
    return (
      <>
        <li
          key={this.props.element.id}
          className="card list-group-item mx-2 my-1 shadow-sm p-0"
          data-toggle="modal"
          data-target={"#modal" + this.props.element.id}
          type="button"
          onClick={this.modalActive}
        >
          {this.isAttachment(this.props.element)}
          <p className="card-title p-2 m-0">{this.props.element.name}</p>
          {this.commentIcon()}
          {this.props.element.desc === ""? "" : <span className="bi bi-justify mx-2"></span>}
        </li>
        <Modal cardData={this.props.element} updateCard={this.props.updateCard}/>
      </>
    );
  }
}
export default OneCard;
