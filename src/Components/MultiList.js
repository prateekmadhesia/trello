import React, { Component } from "react";
import List from "./List";
import "./List.css";
import "./MultiList.css";

class MultiList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newListName: "",
      listFormed: false,
    };
  }

  displayList = () => {
    return this.props.listArray.map((element) => {
      return <List key={element.id} element={element} />;
    });
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.listFormed !== this.state.listFormed ||
      prevProps.listArray.length < this.props.listArray.length
    ) {
      setTimeout(() => {
        this.props.newList();
      }, 1000);
    }
  }

  handelForm = (e) => {
    this.setState({ newListName: e.target.value });
  };

  formSubmit = (e) => {
    if (this.state.newListName === "") {
      return;
    } else {
      this.setState((prev) => {
        return {
          listFormed: !prev.listFormed,
        };
      });
      fetch(
        "https://api.trello.com/1/lists?name=" +
          this.state.newListName +
          "&idBoard=" +
          this.props.boardId +
          "&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
        {
          method: "POST",
        }
      )
        .then((resolve) => {
          console.log(resolve.status);
          return resolve.json();
        })
        .then((data) => {
          console.log(data);
        })
        .catch((err) => {
          console.error("Error on new list post request");
          console.error(err);
        });

      this.setState({
        newListName: "",
      });
    }
  };

  newList = () => {
    return (
      <div className="newList me-3">
        <div className="addList py-2 fs-5">
          <span className=" bi bi-plus icon-2x m-1"></span>
          <a
            className="text-decoration-none text-dark"
            data-bs-toggle="collapse"
            href={"#listInputCollapse"}
            role="button"
            aria-expanded="false"
            aria-controls={"listInputCollapse"}
          >
            Add another list
          </a>
        </div>
        <div className="cardOpen container collapse" id={"listInputCollapse"}>
          <div className="inputField pt-2">
            <input
              className="border-0 shadow-sm rounded form-control"
              placeholder="Enter list title…"
              value={this.state.newListName}
              onChange={this.handelForm}
            />
          </div>
          <div className="inputButtons h-1 py-2 ">
            <button
              type="button"
              className="btn btn-primary opacity-75 btn-sm me-2"
              onClick={this.formSubmit}
              aria-controls={"listInputCollapse"}
              data-bs-toggle="collapse"
              data-bs-target={"#listInputCollapse"}
              aria-expanded="false"
            >
              Add list
            </button>
            <button
              className="bi bi-x-lg mx-2 border-0 shadow-sm"
              aria-controls={"listInputCollapse"}
              data-bs-toggle="collapse"
              data-bs-target={"#listInputCollapse"}
              aria-expanded="false"
            ></button>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="list-div d-flex flex-row flex-nowrap overflow-auto h-100 mt-3 ps-3">
        {this.displayList()}
        {this.newList()}
      </div>
    );
  }
}
export default MultiList;
