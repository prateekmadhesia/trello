import React, { Component } from "react";
import Cards from "./Cards";
import "./List.css";

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newCardName: "",
      cardFormed: false,
    };
  }

  handelForm = (e) => {
    this.setState({ newCardName: e.target.value });
  };
  formSubmit = (e) => {
    if (this.state.newCardName === "") {
      return;
    } else {
      this.setState((prev) => {
        return {
          cardFormed: !prev.cardFormed,
        };
      });
      fetch(
        "https://trello.com/1/cards?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa&idList=" +
          this.props.element.id +
          "&name=" +
          this.state.newCardName,
        {
          method: "POST",
        }
      )
        .then((response) => {
          return response.json();
        })
        .catch((err) => {
          console.error("Error on card post request");
          console.error(err);
        });
      this.setState({ newCardName: "" });
    }
  };

  displayList = () => {
    let element = this.props.element;
    return (
      <div className=" d-inline-block mx-1 h-75">
        <div className="card bg-light" style={{ width: "300px" }}>
          <div className="card-header border-0 p-1">
            <h5 className="px-2 my-1">{element.name}</h5>
          </div>
          <ul className="list-group list-group-flush border-0">
            <Cards list={element} formed={this.state.cardFormed} />
          </ul>
          <div className="d-block position-relative">
            <div className="container addCard pt-1">
              <span className=" bi bi-plus icon-2x m-1"></span>
              <a
                className="text-decoration-none text-dark"
                data-bs-toggle="collapse"
                href={"#card" + element.id}
                role="button"
                aria-expanded="false"
                aria-controls={"card" + element.id}
              >
                Add a card
              </a>
            </div>
            <div
              className="cardOpen container collapse"
              id={"card" + element.id}
            >
              <div className="inputField">
                <textarea
                  className="border-0 shadow-sm rounded p-2 form-control"
                  placeholder="Enter a title for this card…"
                  rows="3"
                  cols="28"
                  value={this.state.newCardName}
                  onChange={this.handelForm}
                ></textarea>
              </div>
              <div className="inputButtons h-1 p-2 ">
                <button
                  type="button"
                  className="btn btn-primary opacity-75 btn-sm me-2"
                  onClick={this.formSubmit}
                  aria-controls={"card" + element.id}
                  data-bs-toggle="collapse"
                  data-bs-target={"#card" + element.id}
                  aria-expanded="false"
                >
                  Add card
                </button>
                <button
                  className="bi bi-x-lg mx-2 border-0 shadow-sm"
                  aria-controls={"card" + element.id}
                  data-bs-toggle="collapse"
                  data-bs-target={"#card" + element.id}
                  aria-expanded="false"
                ></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return <>{this.displayList()}</>;
  }
}
export default List;
