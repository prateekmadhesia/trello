import React, { Component } from "react";
import MultiList from "./MultiList";
import Header from "./Header";
import "./Board.css";

class Board extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listData: "",
    };
  }
  componentDidMount() {
    this.getListData();
  }
  //Getting all list detail
  getListData = () => {
    fetch(
      "https://api.trello.com/1/boards/" +
        this.props.boardArray[0].id +
        "/lists?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ listData: data });
      })
      .catch((err) => {
        console.error("Error in fetching all list of a board");
        console.error(err);
      });
  };
  getWindowDimensions = () => {
    const { innerHeight: height } = window;
    return height;
  };
  getStyle = () => {
    return {
      height: this.getWindowDimensions(),
      background: "url(" + this.props.boardArray[0].prefs.backgroundImage + ")",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top left",
      backgroundSize: "cover",
    };
  };

  render() {
    return (
      <div className="board" style={this.getStyle()}>
        <Header />
        <h4 className="ms-3 my-2">{this.props.boardArray[0].name}</h4>
        {this.state.listData === "" ? (
          <h3>Loading...</h3>
        ) : (
          <MultiList
            listArray={this.state.listData}
            newList={this.getListData}
            boardId={this.props.boardArray[0].id}
          />
        )}
      </div>
    );
  }
}
export default Board;
