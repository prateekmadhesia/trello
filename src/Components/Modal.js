import React, { Component } from "react";
import Cover from "./Cover";
import Description from "./Description";
import DueDate from "./DueDate";

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newComment: "",
      previousComments: [],
      commentFormed: false,
    };
  }
  componentDidMount() {
    this.getComment();
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.commentFormed !== this.state.commentFormed ||
      prevState.previousComments.length < this.state.previousComments.length
    ) {
      setTimeout(() => {
        this.getComment();
      }, 1000);
    }
  }

  getComment = () => {
    fetch(
      "https://api.trello.com/1/cards/" +
        this.props.cardData.id +
        "/actions?filter=commentCard&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa"
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ previousComments: data });
      })
      .catch((err) => {
        console.error("Error on fetching comments");
        console.error(err);
      });
  };

  addComment = (e) => {
    if (this.state.newComment === "") {
      return;
    } else {
      this.setState((prev) => {
        return { commentFormed: !prev.commentFormed };
      });
      fetch(
        "https://api.trello.com/1/cards/" +
          this.props.cardData.id +
          "/actions/comments?text=" +
          this.state.newComment +
          "&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
        {
          method: "POST",
        }
      )
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          this.props.updateCard();
        })
        .catch((err) => {
          console.error("Error on post request of comment");
          console.error(err);
        });
      this.setState({ newComment: "" });
    }
  };

  comments = () => {
    if (this.state.previousComments.length === 0) {
      return "";
    } else {
      return this.state.previousComments.map((commentData) => {
        return (
          <li key={commentData.id}>
            <div className="d-inline-block border border-primary rounded-circle p-1 bg-white">
              <strong> {commentData.memberCreator.initials}</strong>
            </div>
            <h6 className="d-inline-block mx-2">
              {commentData.memberCreator.fullName}
            </h6>
            <div className="container mx-5 border border-info rounded col-10 bg-white px-1">
              <p className="container my-1 p-0 bg-white d-inline-block">
                {commentData.data.text}
              </p>
            </div>
          </li>
        );
      });
    }
  };

  commentForm = (e) => {
    this.setState({ newComment: e.target.value });
  };

  render() {
    return (
      <div
        className="modal fade"
        id={"modal" + this.props.cardData.id}
        tabIndex="-1"
        role="dialog"
        aria-labelledby={"modal" + this.props.cardData.id + "Label"}
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div
              className="modal-header"
              style={{ backgroundColor: "#ebecf0" }}
            >
              <h4
                className="modal-title"
                id="exampleModalLabel"
                style={{ color: "#172b4d" }}
              >
                {this.props.cardData.name}
              </h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body container p-3 px-4 bg-light">
              <DueDate
                cardData={this.props.cardData}
                updateCard={this.props.updateCard}
              />
              <Description
                cardData={this.props.cardData}
                updateCard={this.props.updateCard}
              />
              {/* <Cover
                cardData={this.props.cardData}
                updateCard={this.props.updateCard}
              /> */}

              <div>
                <h5 style={{ color: "#172b4d" }}>
                  {" "}
                  <span className="bi bi-text-left"></span>Activity
                </h5>
              </div>
              <div className="comment-input container mb-1 px-4">
                <input
                  className="form-control shadow-sm col-11"
                  placeholder="Write a comment..."
                  value={this.state.newComment}
                  onChange={this.commentForm}
                />

                <button
                  type="button"
                  className="btn btn-primary btn-sm my-2 shadow-sm "
                  onClick={this.addComment}
                >
                  Comment
                </button>
              </div>
              <ul className="comments list-unstyled container bg-light mb-3">
                {this.comments()}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
