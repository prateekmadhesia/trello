import React, { Component } from "react";
import axios from "axios";
class Cover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      color: [
        "pink",
        "yellow",
        "lime",
        "blue",
        "black",
        "orange",
        "red",
        "purple",
        "green",
      ],
      idAttachmentCover: "6135eacf50265e19000dde49",
      attachment: [
        "6135eacf50265e19000dde17",
        "6135eacf50265e19000dde20",
        "6135eacf50265e19000dde29",
        "6135eacf50265e19000dde40",
        "6135eacf50265e19000dde37",
      ],
      backgroundURL: [
        "https://images.unsplash.com/photo-1633993364598-50b082282d88?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDN8MzE3MDk5fHx8fHwyfHwxNjM0MjcyNTEw&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
        "https://images.unsplash.com/photo-1634093461773-fbbb0b92d689?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDR8MzE3MDk5fHx8fHwyfHwxNjM0MjcyNTEw&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
        "https://images.unsplash.com/photo-1634016793183-c3a5a5c3a48a?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDJ8MzE3MDk5fHx8fHwyfHwxNjM0MTE2NjM3&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
        "https://images.unsplash.com/photo-1633958632241-2edb4ddf42c4?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDV8MzE3MDk5fHx8fHwyfHwxNjM0MjcyNTEw&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
        "https://images.unsplash.com/photo-1634151295857-5581ce6c82d1?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDJ8MzE3MDk5fHx8fHwyfHwxNjM0MjcyNTEw&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
        "https://images.unsplash.com/photo-1634206090782-227f8c84581b?ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDF8MzE3MDk5fHx8fHwyfHwxNjM0MjcyNTEw&ixlib=rb-1.2.1&w=2560&h=2048&q=90",
      ],
    };
  }

  setColor = (e) => {
    let color = e.target.classList[5];
    //let query = { cover: { color: color } };
    axios({
      method: "put",
      url: "https://api.trello.com/1/cards/616a57b2daa8233ae5ba0690?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      data: {
        color: color,
        brightness: "light",
      },
    })
      .then((response) => {
        console.log(response);
      })
      .then(()=>{
        this.props.updateCard();
      })
      .catch((err) => {
        console.error("Error on put request cover");
        console.error(err);
      });

    // fetch(
    //   "https://api.trello.com/1/cards/616a57b2daa8233ae5ba0690?key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
    //   {
    //     method: "PUT",
    //     body: '{ "cover": { "color":"' + color + '", "brightness":"light" } }',
    //   }
    // )
    //   .then((response) => {
    //     console.log(response.status, response.url);
    //     return response.json();
    //   })
    //   .then((data) => {
    //     console.log(data);
    //     this.props.updateCard();
    //   })
    //   .catch((err) => {
    //     console.error("Error on put request cover");
    //     console.error(err);
    //   });
  };

  getColors = () => {
    return this.state.color.map((col) => {
      return (
        <div
          key={col}
          type="button"
          className={"col-1 mx-1 mb-1 rounded shadow-sm " + col}
          style={{ backgroundColor: col, height: "30px" }}
          onClick={this.setColor}
        ></div>
      );
    });
  };

  render() {
    return (
      <div className="mb-2">
        <div>
          <h5 style={{ color: "#172b4d" }}>
            {" "}
            <span className="bi bi-image"></span> Cover
          </h5>
        </div>
        <div className="accordion container mx-2 col-11" id="accordionExample">
          <div className="accordion-item shadow-sm me-3 ms-1">
            <h2 className="accordion-header" id="headingTwo">
              <button
                className="accordion-button collapsed p-2"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target={"#collapseTwo" + this.props.cardData.id}
                aria-expanded="false"
                aria-controls={"collapseTwo" + this.props.cardData.id}
              >
                Select cover
              </button>
            </h2>
            <div
              id={"collapseTwo" + this.props.cardData.id}
              className="accordion-collapse collapse"
              aria-labelledby="headingTwo"
              data-bs-parent="#accordionExample"
            >
              <div className="accordion-body">
                <div className="colors">
                  <h6>Colors:</h6>
                  <div className="row container">{this.getColors()}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Cover;
