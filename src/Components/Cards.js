import React, { Component } from "react";
import OneCard from "./OneCard";

class Cards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      card: "",
    };
  }
  componentDidMount() {
    this.getCards();
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.formed !== this.props.formed ||
      prevState.card.length < this.state.card.length
    ) {
      setTimeout(() => {
        this.getCards();
      }, 1000);
    }
  }

  getCards = () => {
    fetch(
      "https://api.trello.com/1/lists/" +
        this.props.list.id +
        "/cards?token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa&key=bbe62e79362df90b6e3d5556aa941c73",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ card: data });
      })
      .catch((err) => {
        console.error("Error in fetching cards of a list");
        console.error(err);
      });
  };

  displayCard = () => {
    return this.state.card.map((element) => {
      return <OneCard key={element.id} element={element} updateCard={this.getCards}/>;
    });
  };

  render() {
    return (
      <>{this.state.card === "" ? <h4>Loading...</h4> : this.displayCard()}</>
    );
  }
}
export default Cards;
