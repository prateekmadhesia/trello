import React, { Component } from "react";

class Description extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prevDescription: "",
    };
  }

  componentDidMount() {
    this.getDescription();
  }

  getDescription = () => {
    fetch(
      "https://api.trello.com/1/cards/" +
        this.props.cardData.id +
        "/desc?token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa&key=bbe62e79362df90b6e3d5556aa941c73"
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ prevDescription: data._value });
      })
      .catch((err) => {
        console.error("Error on fetching comments");
        console.error(err);
      });
  };

  descriptionForm = (e) => {
    this.setState({ prevDescription: e.target.value });
  };
  descriptionSubmit = (e) => {
    fetch(
      "https://api.trello.com/1/cards/" +
        this.props.cardData.id +
        "/?desc=" +
        this.state.prevDescription +
        "&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.props.updateCard();
      })
      .catch((err) => {
        console.error("Error on put request comment");
        console.error(err);
      });
  };

  render() {
    return (
      <div className ="mb-2">
        <div>
          <h5 style={{ color: "#172b4d" }}>
            {" "}
            <span className="bi bi-justify"></span> Description
          </h5>
        </div>
        <div className="container">
          {this.state.prevDescription === "" ? (
            ""
          ) : (
            <p className="container m-0 fs-6">{this.state.prevDescription}</p>
          )}
          <div>
            <div className="d-block">
              <div className="container addCard pt-1 bg-light mt-1 ">
                <span className=" bi bi-plus icon-2x m-1 "></span>
                <a
                  className="text-decoration-none text-dark fs-5"
                  data-bs-toggle="collapse"
                  href={"#desc" + this.props.cardData.id}
                  role="button"
                  aria-expanded="false"
                  aria-controls={"desc" + this.props.cardData.id}
                >
                  <span>
                  {this.state.prevDescription === ""
                    ? "Add description"
                    : "Edit"}
                  </span>
                </a>
              </div>
              <div
                className="cardOpen container collapse bg-light"
                id={"desc" + this.props.cardData.id}
              >
                <div className="inputField bg-light">
                  <textarea
                    className="border-0 shadow-sm rounded p-2 form-control col-11"
                    placeholder="Add a more detailed description…"
                    rows="2"
                    cols="20"
                    value={this.state.prevDescription}
                    onChange={this.descriptionForm}
                  ></textarea>
                </div>
                <div className="inputButtons h-1 p-2 ">
                  <button
                    type="button"
                    className="btn btn-primary opacity-2 opacity-75 btn-sm me-2 shadow-sm "
                    onClick={this.descriptionSubmit}
                    aria-controls={"desc" + this.props.cardData.id}
                    data-bs-toggle="collapse"
                    data-bs-target={"#desc" + this.props.cardData.id}
                    aria-expanded="false"
                  >
                    Save
                  </button>
                  <button
                    className="bi bi-x-lg mx-2 border-0 shadow-sm"
                    aria-controls={"desc" + this.props.cardData.id}
                    data-bs-toggle="collapse"
                    data-bs-target={"#desc" + this.props.cardData.id}
                    aria-expanded="false"
                  ></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Description;
