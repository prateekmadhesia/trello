import React, { Component } from "react";
import './Header.css';

class Header extends Component {
  render() {
    return (
      <header className="head">
        <nav className="navbar navbar-expand-md navbar-light">
          <div className="container-fluid">
            <a href="." className="navbar-brand">
              <img
                src="https://a.trellocdn.com/prgb/dist/images/header-logo-spirit-loading.87e1af770a49ce8e84e3.gif"
                alt="logo"
                width="150"
                height="30"
                className="d-inline-block align-text-top"
              />
            </a>
          </div>
        </nav>
      </header>
    );
  }
}
export default Header;
