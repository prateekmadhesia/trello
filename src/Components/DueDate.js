import React, { Component } from "react";

class DueDate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: "",
    };
  }
  componentDidMount() {
    if (this.props.cardData.due === null) {
      return "";
    } else {
      this.setState({ date: this.props.cardData.due });
    }
  }

  setDate = () => {
    if(this.state.date === ""){
      return;
    }
    fetch(
      "https://api.trello.com/1/cards/" +
        this.props.cardData.id +
        "/?due=" +
        this.state.date+
        "&dueComplete=false&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.props.updateCard();
      })
      .catch((err) => {
        console.error("Error on put request of set or update date");
        console.error(err);
      });
  };

  setDateTime = (e) => {
    let date = e.target.value;
    let dateTime = new Date().toISOString();
    let newDateTime = date + dateTime.slice(10);
    console.log(newDateTime);
    this.setState({ date: newDateTime });
  };

  getButton = () => {
    if (this.props.cardData.due === null) {
      return (
        <button
          type="button"
          className="btn btn-primary btn-sm mx-2 shadow-sm "
          onClick={this.setDate}
        >
          Set
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary btn-sm mx-2 shadow-sm "
          onClick={this.setDate}
        >
          Change
        </button>
      );
    }
  };
  complete = () => {
    fetch(
      "https://api.trello.com/1/cards/" +
        this.props.cardData.id +
        "/?dueComplete=" +
        !this.props.cardData.dueComplete +
        "&key=bbe62e79362df90b6e3d5556aa941c73&token=622f7d4261eea1525fcbe498f2797ee359a8336b5bf97ef0415eb49db85143aa",
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.props.updateCard();
      })
      .catch((err) => {
        console.error("Error on put request of dueComplete");
        console.error(err);
      });
  };

  getDate = () => {
    if (this.props.cardData.due === null) {
      return "";
    } else {
      if (this.props.cardData.dueComplete === true) {
        return (
          <div className="d-inline-block">
            <h6 className="d-inline-block">
              <s>{new Date(this.state.date).toUTCString()}</s>
            </h6>
            <span>
              <button
                type="button"
                className="btn btn-success btn-sm mx-2 shadow-sm "
                onClick={this.complete}
              >
                Complete
              </button>
            </span>
          </div>
        );
      } else {
        return (
          <div className="d-inline-block">
            <h6 className="d-inline-block">
              {new Date(this.state.date).toUTCString()}
            </h6>
            <span>
              <button
                type="button"
                className="btn btn-warning btn-sm mx-2 shadow-sm "
                onClick={this.complete}
              >
                Due
              </button>
            </span>
          </div>
        );
      }
    }
  };

  render() {
    return (
      <div className="mb-3">
        <div>
          <h5 style={{ color: "#172b4d" }}>
            <span className="bi bi-calendar3 "></span> Due Date
          </h5>
        </div>
        <div className="container px-4 py-1">
        {this.getDate()}
          <input className="border border-danger" type="date" onChange={this.setDateTime} />
          <span>{this.getButton()}</span>
        </div>
      </div>
    );
  }
}
export default DueDate;
